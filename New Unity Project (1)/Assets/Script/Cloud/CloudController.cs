﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.01f, 0, 0);   //左に-0.1f動く

        if (transform.position.x < -10.0f)    //画面外に出たら破壊
        {
            Destroy(gameObject);
        }
    }
}
