﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudGenerator : MonoBehaviour
{
    public GameObject kumooji;
    float span = 1.9f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        if (delta > span)
        {
            delta = 0;
            GameObject go = Instantiate(kumooji) as GameObject;
            int px = Random.Range( 3, 6);
            go.transform.position = new Vector3(11, px, 0);
        }
    }
}
