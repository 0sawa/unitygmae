﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoveryGenerator : MonoBehaviour
{
    public GameObject RecoveryPrefab;
    float span = 5.0f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        if (delta > span)
        {
            delta = 0;
            GameObject go = Instantiate(RecoveryPrefab) as GameObject;
            int px = Random.Range(-5, 6);
            go.transform.position = new Vector3(11, px, 0);
        }
    }
}
