﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoveryController : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.05f, 0, 0);   //左に-0.1f動く

        if (transform.position.x < -10.0f)    //画面外に出たら破壊
        {
            Destroy(gameObject);
        }

        //あたり判定
        Vector2 p1 = transform.position;
        Vector2 p2 = player.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;
        float r2 = 1.0f;

        if (d < r1 + r2)
        {
            Destroy(gameObject);

            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().RecoveryHp();

        }
    }
}
