﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.UpArrow))   //↑キー
        {
            transform.Translate(0, 0.03f, 0); 
        }

        if(Input.GetKey(KeyCode.DownArrow)) //↓キー   
        {
            transform.Translate(0, -0.03f, 0);
        }
    }
}
