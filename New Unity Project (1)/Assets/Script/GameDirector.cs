﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    GameObject hpGauge;
    GameObject Score;

    int x = 0;
    int y = 9;
    int score = 0;

    // Start is called before the first frame update
    void Start()
    {
        hpGauge = GameObject.Find("hpGauge");
        Score = GameObject.Find("Score");
    }

    // Update is called once per frame
    void Update()
    {
        if (y < x)
        {
            SceneManager.LoadScene("GameOverScene"); //xよりyが小さくなったら次のシーン
        }
    }

    public void DecreaseHp()
    {
        hpGauge.GetComponent<Image>().fillAmount -= 0.1f; //HPゲージが減る

        x += 1;
    }

    public void IncreaseScore()
    {
        
        score += 10;
        Score.GetComponent<Text>().text = "スコア:" + score;　//スコアを加算
    }

    public void RecoveryHp()
    {
        hpGauge.GetComponent<Image>().fillAmount += 0.1f;　//HPゲージ増える
        x -= 1;
    }
}
